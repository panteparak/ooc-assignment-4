package io.muic.ooc.a4.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


public class UsersUtils {
  public static boolean isSelf(HttpServletRequest req, HttpServletResponse res, String id){
    List<Map<String, String>> jwtParams = CookieUtil.getCookieParams(req, res);
    for (Map<String, String> item : jwtParams){
      if (item.get("id").equalsIgnoreCase(id))
        return true;
    }
    return false;
  }
}
