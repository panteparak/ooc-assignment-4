package io.muic.ooc.a4.utils;


public class TemplatesUtil {
  public static class Login {
    public static final String TEMPLATE_LOCATION = "/templates/login.jsp";
    public static final String SERVLET_NAME = "LoginServlet";
    public static final String SERVLET_PATH = "/login";
  }

  public static class Users {
    public static final String TEMPLATE_LOCATION = "/templates/users.jsp";
    public static final String SERVLET_NAME = "UsersServlet";
    public static final String SERVLET_PATH = "/users";
  }

  public static class AddUsers {
    public static final String TEMPLATE_LOCATION = "/templates/adduser.jsp";
    public static final String SERVLET_NAME = "AddUsersServlet";
    public static final String SERVLET_PATH = "/users/add";
  }

  public static class Logout {
    public static final String TEMPLATE_LOCATION = "";
    public static final String SERVLET_NAME = "LogoutServlet";
    public static final String SERVLET_PATH = "/logout";
  }

  public static class EditUser {
    public static final String TEMPLATE_LOCATION = "/templates/edituser.jsp";
    public static final String SERVLET_NAME = "EditUserServlet";
    public static final String SERVLET_PATH = "/users/edit/*";
  }

  public static class RemoveUser {
    public static final String TEMPLATE_LOCATION = "";
    public static final String SERVLET_NAME = "LogoutServlet";
    public static final String SERVLET_PATH = "/users/remove/*";
  }

  public static class Home {
    public static final String TEMPLATE_LOCATION = "";
    public static final String SERVLET_NAME = "HomeServlet";
    public static final String SERVLET_PATH = "/";
  }

  public static class ResetPassword {
    public static final String TEMPLATE_LOCATION = "/templates/resetpassword.jsp";
    public static final String SERVLET_NAME = "HomeServlet";
    public static final String SERVLET_PATH = "/users/resetpassword/*";
  }

}
