package io.muic.ooc.a4.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SessionValidatorUtil {

  //  Session Validator Failed
  public static boolean validateCredentials(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    if (!CookieUtil.verifyCookie(req, resp)) {
      CookieUtil.removeCookie(req, resp);
      resp.sendRedirect(resp.getContentType() + "/login");
      return false;
    }
    return true;
  }
}
