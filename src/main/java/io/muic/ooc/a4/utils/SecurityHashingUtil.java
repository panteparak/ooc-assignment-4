package io.muic.ooc.a4.utils;

import org.mindrot.jbcrypt.BCrypt;

import java.util.concurrent.TimeUnit;


public final class SecurityHashingUtil {
  public static final String hashPassword(String plainPassword) {
    return BCrypt.hashpw(plainPassword, BCrypt.gensalt(12));
  }

  public static final boolean verifyPassword(String securePassword, String plainPassword) {
    try {
      return BCrypt.checkpw(plainPassword, securePassword);
    } catch (IllegalArgumentException e){
      return false;
    }
  }

  public static void main(String[] args) {
//
    String plainpass1 = "helloworld";
    String plainpass2 = "hgbwbgirnkgnrngkrnkgrniwgngw";
    long start = System.nanoTime();
    String a = hashPassword(plainpass2);
    long end = System.nanoTime();
//
    System.out.println(TimeUnit.NANOSECONDS.toMillis(end - start));
//    System.out.println(verifyPassword(a, plainpass2));


//    System.out.println(verifyPassword("12345", "$2a$12$cLyqSQB2zPw1cWJeY6qCqeRnvicc44.drAekS/3v8xUcFr9lGcfye"));
  }
}
