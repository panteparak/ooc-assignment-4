package io.muic.ooc.a4.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;


public final class JWTUtil {

  //      Just temporary
  private static final String KEY = "YW+QFQTGAcxTu6/c/v4UIdMFIY8sUFD+xMQXBB8ZVlsfFaqEE1DN1j+5k2VIXNBc" +
          "okNKgTam2LML76vH9LuofOMBtV8lBVf6fKraPbjV3rHh7/w7CZdtCEeO6CIq8D7z" +
          "M+bJoHDE206/jXkd4OGjlfYEuBYy2+gD3USC5GC0dhI91Y0TSqw4EyaJhv35V4WJ" +
          "dpGMCrW2bRVkNLd0B4C0VTOg9isX418s2APEK80/+KdpCasqPQpVunsJFQvs6UPx" +
          "L1CyqD8jYRwvfFHimxT5UxVANuLiNxUcCTo/7D6PjP972Pjo9ummP3WNIPStSzUu" +
          "M1ZVBa2ggajkHnZlHHZJrxTBymh0oS6LmBOOAW8VD5lzneVo61n2d6BTaO65LpqC" +
          "z9WgwWuJVTPe931SPEsraVk7qXUmZ8Jcz2XvN8YmDPQ5cyLmvq+Osp5M6KmDdZnt" +
          "s3fXHYDYuhnBPYTxtcdL/M1icKHWSjfNFYk5QQQMMo0g90moPrwUnmuurbokErki" +
          "uhouosp2KHl4HHe7p1yc2jfHV5AL3jrYkz7nToQ5+ZvFDQdKU6qNhC5crIjKyv08" +
          "DE2SYnV7404JKbO0LqYkWze158Y8PJwdx3UGgwjRG4EjWlEYqXEkgswk4GVbmZQt" +
          "ZgF8sRdyoPJwzu+XFIpqVf9czFp/UDOkgk5tSsNt3X3Tf/KEfYmwep6zDzZk4CLP" +
          "DE0ffDVGmlGCsfXV8bTsxQpSpGSd7L5SaRq88STe8Cvuk/3WSxxMCHPCkgHaY7z4" +
          "kJ3UqrYk+ECr2DeMGMJxcV/mpYqYEESgWmQNIiUfThYn2cv4zTbPIwR+WlRBl5BP" +
          "5Sski3aNHs+SLWT/vyNEi2um75g36Hn4b5nPZYpinGtKGHVrY5LoC/do+JaZaVAW" +
          "YyVtz5USHLKKYNBBSIYmNhX1+AIC8/C5HdCRHDnRedd1ATk6VAoZw/iM3hkjBoTT" +
          "nLOD9cNDLlo0MunbMGoPmZID0FtRzHzXaXj++mGyTa7wDaRTIAEYcS0hRhieq6Wu" +
          "RDxzwa0PkNrz96RhLGRhZwecb03zOCSqi0YeGyRzaGBWkqYhHnhgL1ibQFnXKDyD" +
          "rRUJ9D62BYEe7vTo2mx5vOAM/VtCKB0+3uzpEOkZVcbuv2aYh/tb7ZuRhCZ1Mv1l" +
          "FywIeI+Jzr+LUsoOhdBVCAwRD0kNULYC043Kx+WCM8P2evmm7gadwBGE0Gbx+cXg" +
          "1+08BTzIti8OiaBo0fLCuE6OdVqz/sr+U5jChCWrNkuh1i9lVDcvCkSagZkyKCXK" +
          "nTpH+FagjEoN0EWUsS7F3zReu9+Ry2o/pSN8eulNCJfp4tmk9tO5OoOSzvtL/OrN" +
          "OJ/yAb4ZpNadvWrrzWQiYw==";

  public final static String generateToken(String id, String username) {
    Calendar calender = Calendar.getInstance();
    calender.setTime(new Date());

//    7 Days expiration Period
    calender.add(Calendar.DAY_OF_YEAR, 7);
    Date expDate = calender.getTime();

    String token = null;
    try {
      token = JWT.create()
              .withIssuedAt(new Date())
              .withIssuer("XPLOT1ON")
              .withIssuedAt(new Date())
              .withClaim("id", id)
              .withClaim("username", username)
              .withExpiresAt(expDate)
              .sign(Algorithm.HMAC512(KEY));

    } catch (UnsupportedEncodingException e) {
      return token;
    }
    return token;
  }

  public final static Boolean verifyToken(String token){
    try {
      JWTVerifier verifier = JWT.require(Algorithm.HMAC512(KEY))
              .acceptLeeway(10)
              .build();
      verifier.verify(token);
    } catch (UnsupportedEncodingException e){
      return false;
    } catch (JWTVerificationException e){
      return false;
    }

    return true;
  }

  public final static HashMap<String, String> getClaims(String jwtText){
    HashMap<String, String> out = new HashMap<String, String>();
    if (verifyToken(jwtText)){
      try {

        JWT jwt = JWT.decode(jwtText);
        out.put("id", jwt.getClaim("id").isNull() ? "" : jwt.getClaim("id").asString());
        out.put("username", jwt.getClaim("username").isNull() ? "" : jwt.getClaim("username").asString());

      } catch (JWTDecodeException e){
        e.printStackTrace();
      }
    }
    return out;

  }

  public final static void main(String[] args) throws InterruptedException {
    String token = generateToken("1", "pan");
//    String token = "ABC";
    Thread.sleep(TimeUnit.SECONDS.toMillis(5));
    System.out.println(verifyToken(token));
  }
}
