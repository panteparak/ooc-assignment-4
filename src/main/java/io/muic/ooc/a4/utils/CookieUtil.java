package io.muic.ooc.a4.utils;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


public class CookieUtil {
  public static void setCookie(HttpServletRequest req, HttpServletResponse res, String accountID, String username) {
    Cookie cookie = new Cookie("jwt", JWTUtil.generateToken(accountID, username));
    res.addCookie(cookie);
  }

  public static boolean verifyCookie(HttpServletRequest req, HttpServletResponse res){
    Cookie[] cookies = req.getCookies();
    for (int i = 0; cookies != null && i < cookies.length; i++)
      if (JWTUtil.verifyToken(cookies[i].getValue()))
        return true;
    return false;
  }

  public static void removeCookie(HttpServletRequest req, HttpServletResponse res) {
    Cookie[] cookies = req.getCookies();
    for (int i = 0; cookies != null && i < cookies.length; i++) {
      Cookie cookie = cookies[i];
      cookie.setMaxAge(0); // 0 will delete cookie (According to API Docs)
      res.addCookie(cookie);
    }
  }

  public static List<Map<String,String>> getCookieParams(HttpServletRequest req, HttpServletResponse res){
    List<Map<String, String>> out = new ArrayList<Map<String,String>>();
    Cookie[] cookies = req.getCookies();
    for (int i = 0; i < cookies.length; i++){
      if (JWTUtil.verifyToken(cookies[i].getValue())){
        out.add(JWTUtil.getClaims(cookies[i].getValue()));
      }
    }
    return out;
  }
}
