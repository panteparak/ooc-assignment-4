package io.muic.ooc.a4.db.mysql.model;

/**
 * Created by panteparak on 2/16/17.
 */
public class UserModel {
  private String id;
  private String username;
  private String hashPassword;
  private String firstname;
  private String lastname;

  public UserModel(String id, String username, String hashPassword, String firstname, String lastname) {
    this.id = id;
    this.username = username;
    this.hashPassword = hashPassword;
    this.firstname = firstname;
    this.lastname = lastname;
  }

  public String getId() {
    return id;
  }

  public String getUsername() {
    return username;
  }

  public String getHashPassword() {
    return hashPassword;
  }

  public String getFirstname() {
    return firstname;
  }

  public String getLastname() {
    return lastname;
  }
}
