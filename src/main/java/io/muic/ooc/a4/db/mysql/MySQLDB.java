package io.muic.ooc.a4.db.mysql;

import io.muic.ooc.a4.db.mysql.model.UserModel;
import io.muic.ooc.a4.utils.SecurityHashingUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySQLDB {
  private static String dbURL = "jdbc:mysql://localhost/ooc-webapp?useSSL=false";
  private static String dbUser = "ooc";
  private static String dbPass = "oociccs330";
  private static Connection conn = null;
  private ResultSet resultSet;
  private PreparedStatement preparedStatement;

  private Connection getConn() {
    try {
      Class.forName("com.mysql.jdbc.Driver");
      if (conn == null || conn.isClosed()) {
        conn = DriverManager.getConnection(dbURL, dbUser, dbPass);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException ex){
      ex.printStackTrace();
    }

    System.out.println("Connection is NULL: " + conn == null);
    return conn;
  }

  public List<HashMap<String, String>> getUsers(){
    List<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
    try {
      String query = "SELECT id, username, firstname, lastname FROM Users;";
      preparedStatement = getConn().prepareStatement(query);
      ResultSet rs = preparedStatement.executeQuery();

      while (rs.next()){
        HashMap<String, String> temp = new HashMap<String, String>();
        temp.put("id", rs.getString("id"));
        temp.put("username", rs.getString("username"));
        temp.put("firstname", rs.getString("firstname"));
        temp.put("lastname", rs.getString("lastname"));
        data.add(temp);
      }
    } catch (SQLException e){
      e.printStackTrace();
    }

    return data;
  }

  public boolean validateLogin(String inputUser, String inputPass) {
    try {
      preparedStatement = getConn().prepareStatement("SELECT hashpassword FROM Users WHERE username = ?;");
      preparedStatement.setString(1, inputUser);
      resultSet = preparedStatement.executeQuery();
      if (resultSet.first()) {
        String hashPassword = resultSet.getString("hashpassword");

        return SecurityHashingUtil.verifyPassword(hashPassword, inputPass);
      }
    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      try {
        if (resultSet != null && !resultSet.isClosed()) resultSet.close();
        if (preparedStatement != null && !preparedStatement.isClosed()) preparedStatement.close();
      } catch (SQLException e) {
        return false;
      }
    }
    return false;
  }

  public boolean createNewUser(String username, String plainPassword, String firstname, String lastname) {
    try {
      String hashPassword = SecurityHashingUtil.hashPassword(plainPassword);

      preparedStatement = getConn().prepareStatement("INSERT INTO Users (username, hashpassword, firstname, lastname) VALUES(?,?,?,?);");
      preparedStatement.setString(1, username);
      preparedStatement.setString(2, hashPassword);
      preparedStatement.setString(3, firstname);
      preparedStatement.setString(4, lastname);
      preparedStatement.executeUpdate();
      return true;
    } catch (SQLException e) {
      return false;
    } finally {
      try {
        if (preparedStatement != null && !preparedStatement.isClosed()) preparedStatement.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

  public Map<String, String> getUserById(int id){
    return getUserById(String.valueOf(id));
  }

  public Map<String, String> getUserById(String id){
    String qry = "SELECT id, firstname, lastname FROM Users WHERE ID = ?;";
    Map<String, String> user = new HashMap<>();

    try {
      preparedStatement = getConn().prepareStatement(qry);
      preparedStatement.setString(1, id);
      ResultSet  rs = preparedStatement.executeQuery();

      if (rs.next()) {
        user.put("id", rs.getString("id"));
        user.put("firstname", rs.getString("firstname"));
        user.put("lastname", rs.getString("lastname"));
      }
    } catch (SQLException e){
      e.printStackTrace();
    }
    return user;
  }

  public HashMap<String, String> getUserByUsername(String username){
    String qry = "SELECT id, username, firstname, lastname FROM Users WHERE username = ?;";

    HashMap<String, String> map = new HashMap<String, String>();
    try {
      preparedStatement = getConn().prepareStatement(qry);
      preparedStatement.setString(1, username);
      ResultSet rs = preparedStatement.executeQuery();

      if (rs.next()){
        map.put("id", rs.getString("id"));
        map.put("username", rs.getString("username"));
        map.put("firstname", rs.getString("firstname"));
        map.put("lastname", rs.getString("lastname"));
      }

    }catch (SQLException e){
      e.printStackTrace();
    }

    return map;
  }

  public void updateUserInfo(String id, String firstname, String lastname){
    String qry = "UPDATE Users SET firstname = ?, lastname = ? WHERE id = ?;";
    try {
      preparedStatement = getConn().prepareStatement(qry);
      preparedStatement.setString(3, id);
      preparedStatement.setString(1, firstname);
      preparedStatement.setString(2, lastname);
      preparedStatement.executeUpdate();
    } catch (SQLException e){
      e.printStackTrace();
    }
  }

  public void removeUserById(String id){
    String qry = "DELETE FROM Users WHERE id = ?";
    try {
      preparedStatement = getConn().prepareStatement(qry);
      preparedStatement.setString(1, id);
      preparedStatement.executeUpdate();
    } catch (SQLException e){
      e.printStackTrace();
    }
  }

  public void setPassword(String id, String password){
    String qry = "UPDATE Users SET hashpassword = ? WHERE id = ?;";

    String hashPassword = SecurityHashingUtil.hashPassword(password);
    try {
      preparedStatement = getConn().prepareStatement(qry);
      preparedStatement.setString(2, id);
      preparedStatement.setString(1, hashPassword);
      preparedStatement.executeUpdate();
    } catch (SQLException e){
      e.printStackTrace();
    }

  }

  public static void main(String[] args) {
//    MySQLDB mySQLDB = new MySQLDB();
//    System.out.println(mySQLDB.createNewUser("peter", "pan", "Peter", "Pan"));
//    System.out.println(mySQLDB.createNewUser("ham", "muel", "ham", "muel"));

//    mySQLDB.setPassword("1", "pan");
//    mySQLDB.setPassword("3", "12345");
//    mySQLDB.setPassword("5", "muel");


//    System.out.println(mySQLDB.validateLogin("admin", "12345"));

//    mySQLDB.getUserById("1");
  }
}
