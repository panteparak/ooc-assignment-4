package io.muic.ooc.a4.servlet;

import io.muic.ooc.a4.utils.CookieUtil;
import io.muic.ooc.a4.utils.TemplatesUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(TemplatesUtil.Home.SERVLET_PATH)
public class HomeServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    if (CookieUtil.verifyCookie(req, resp))
      resp.sendRedirect(TemplatesUtil.Users.SERVLET_PATH);
    else
      resp.sendRedirect(TemplatesUtil.Login.SERVLET_PATH);
  }
}
