package io.muic.ooc.a4.servlet;

import io.muic.ooc.a4.db.mysql.MySQLDB;
import io.muic.ooc.a4.utils.SessionValidatorUtil;
import io.muic.ooc.a4.utils.TemplatesUtil;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(TemplatesUtil.ResetPassword.SERVLET_PATH)
public class ResetPasswordServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    if (!SessionValidatorUtil.validateCredentials(req, resp)) return;
    String[] params = StringUtils.replace(req.getRequestURI(), "/users/resetpassword/", "").split("/", 2);

    if (params.length < 1 && params[0].trim().length() == 0) {
      resp.sendRedirect(TemplatesUtil.Users.SERVLET_PATH);
      return;
    }

    req.setAttribute("userid", params[0]);
    req.getRequestDispatcher(TemplatesUtil.ResetPassword.TEMPLATE_LOCATION).forward(req,resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//    String[] params = StringUtils.replace(req.getRequestURI(), "/users/resetpassword/", "").split("/", 2);
    MySQLDB mySQLDB;


    String password = req.getParameter("password");
    String id = req.getParameter("id");


    if (StringUtils.isNotBlank(password) && StringUtils.isNotBlank(id)){
      mySQLDB = new MySQLDB();
      System.out.println("Change Pass");
      System.out.println(id);
      System.out.println(password);
      mySQLDB.setPassword(id, password);
      resp.sendRedirect(TemplatesUtil.Users.SERVLET_PATH);
    } else {
      req.setAttribute("error", "Password cannon be blank");
      req.getRequestDispatcher(TemplatesUtil.ResetPassword.TEMPLATE_LOCATION).include(req, resp);
    }
  }
}
