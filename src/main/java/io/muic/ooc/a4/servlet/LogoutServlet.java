package io.muic.ooc.a4.servlet;

import io.muic.ooc.a4.utils.CookieUtil;
import io.muic.ooc.a4.utils.SessionValidatorUtil;
import io.muic.ooc.a4.utils.TemplatesUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(TemplatesUtil.Logout.SERVLET_PATH)
public class LogoutServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    if (!SessionValidatorUtil.validateCredentials(req, resp)) return;
    CookieUtil.removeCookie(req, resp);
    resp.sendRedirect("/");
  }
}
