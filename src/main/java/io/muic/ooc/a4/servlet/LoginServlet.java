package io.muic.ooc.a4.servlet;

import io.muic.ooc.a4.db.mysql.MySQLDB;
import io.muic.ooc.a4.utils.CookieUtil;
import io.muic.ooc.a4.utils.TemplatesUtil;
import org.apache.commons.lang.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@WebServlet(TemplatesUtil.Login.SERVLET_PATH)
public class LoginServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    RequestDispatcher requestDispatcher = req.getRequestDispatcher(TemplatesUtil.Login.TEMPLATE_LOCATION);
    requestDispatcher.forward(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String username = req.getParameter("username");
    String password = req.getParameter("password");

    MySQLDB mySQLDB = new MySQLDB();

    boolean paramsNotNull = StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password);
    boolean isValid = mySQLDB.validateLogin(username, password);

    if (paramsNotNull && isValid){

      HashMap<String, String> data = mySQLDB.getUserByUsername(username);

      if (data.keySet().contains("id") && data.keySet().contains("username")) {
        CookieUtil.setCookie(req, resp, data.get("id"), data.get("username"));
        resp.sendRedirect("/users");
        return;
      }
    }
    req.setAttribute("error", "Invalid Credentials");
    req.getRequestDispatcher(TemplatesUtil.Login.TEMPLATE_LOCATION).forward(req, resp);
  }


  private boolean inputValidator(String input) {
    return ((input != null || input.trim().isEmpty()));
  }
}
