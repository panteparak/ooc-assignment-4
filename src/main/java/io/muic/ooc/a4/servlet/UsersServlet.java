package io.muic.ooc.a4.servlet;

import io.muic.ooc.a4.db.mysql.MySQLDB;
import io.muic.ooc.a4.utils.CookieUtil;
import io.muic.ooc.a4.utils.SessionValidatorUtil;
import io.muic.ooc.a4.utils.TemplatesUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(TemplatesUtil.Users.SERVLET_PATH)
public class UsersServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    if (!SessionValidatorUtil.validateCredentials(req, resp)) return;
    MySQLDB mySQLDB = new MySQLDB();

    System.out.println(CookieUtil.getCookieParams(req, resp));
    if (CookieUtil.getCookieParams(req, resp).size() == 1){
      req.setAttribute("currentUser", CookieUtil.getCookieParams(req, resp).get(0).get("username"));
    }else {
      req.setAttribute("currentUser", "More Than 1 user");
    }
    req.setAttribute("lstUsers", mySQLDB.getUsers());
    req.getRequestDispatcher(TemplatesUtil.Users.TEMPLATE_LOCATION).forward(req, resp);
  }
}
