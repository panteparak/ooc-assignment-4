package io.muic.ooc.a4.servlet;

import io.muic.ooc.a4.utils.CookieUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

//@WebFilter("/*")
public class FilterServlet implements Filter {
  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse res = (HttpServletResponse) response;

    List<String> lstRoute = Arrays.asList("","/favicon.ico","/users", "/login", "/users/add", "/users/edit", "/users/remove");

    if (!lstRoute.contains(req.getRequestURI())){
      res.sendRedirect("/");
    }
  }

  @Override
  public void destroy() {

  }

  public boolean checkRedirect(HttpServletRequest req, HttpServletResponse resp){
    return CookieUtil.verifyCookie(req, resp);
  }

}
