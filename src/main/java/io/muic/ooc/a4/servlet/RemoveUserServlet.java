package io.muic.ooc.a4.servlet;

import io.muic.ooc.a4.db.mysql.MySQLDB;
import io.muic.ooc.a4.utils.SessionValidatorUtil;
import io.muic.ooc.a4.utils.TemplatesUtil;
import io.muic.ooc.a4.utils.UsersUtils;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(TemplatesUtil.RemoveUser.SERVLET_PATH)
public class RemoveUserServlet extends HttpServlet{
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    System.out.println("Hi");
    if (!SessionValidatorUtil.validateCredentials(req, resp)) return;
    String[] params = StringUtils.replace(req.getRequestURI(), "/users/remove/", "").split("/", 2);

    if ((params.length < 1 && params[0].trim().length() == 0)|| UsersUtils.isSelf(req, resp, params[0])) {
      resp.sendRedirect(TemplatesUtil.Users.SERVLET_PATH);
      return;
    }

    MySQLDB mySQLDB = new MySQLDB();
//    System.out.println("Removing" + params[0]);
    mySQLDB.removeUserById(params[0]);

    resp.sendRedirect(TemplatesUtil.Users.SERVLET_PATH);

  }


}
