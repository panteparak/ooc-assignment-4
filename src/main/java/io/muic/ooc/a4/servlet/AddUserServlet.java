package io.muic.ooc.a4.servlet;

import io.muic.ooc.a4.db.mysql.MySQLDB;
import io.muic.ooc.a4.utils.SessionValidatorUtil;
import io.muic.ooc.a4.utils.TemplatesUtil;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(TemplatesUtil.AddUsers.SERVLET_PATH)
public class AddUserServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    if (!SessionValidatorUtil.validateCredentials(req, resp)) return;
    req.getRequestDispatcher(TemplatesUtil.AddUsers.TEMPLATE_LOCATION).include(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    if (!SessionValidatorUtil.validateCredentials(req, resp)) return;

    String username = req.getParameter("username");
    String password = req.getParameter("password");
    String firstname = req.getParameter("firstname");
    String lastname = req.getParameter("lastname");

    boolean isValid = StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password) && StringUtils.isNotBlank(firstname) && StringUtils.isNotBlank(lastname);

    MySQLDB mySQLDB;
    if (isValid){
      mySQLDB = new MySQLDB();
      boolean success = mySQLDB.createNewUser(username.trim(), password.trim(), firstname.trim(), lastname.trim());

      if (success){
        resp.sendRedirect(TemplatesUtil.Users.SERVLET_PATH);
      } else {
        req.setAttribute("error", "Username already Exist");
        req.getRequestDispatcher(TemplatesUtil.AddUsers.TEMPLATE_LOCATION).forward(req, resp);
      }
    }
  }
}
