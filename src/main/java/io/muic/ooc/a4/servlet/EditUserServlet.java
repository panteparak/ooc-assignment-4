package io.muic.ooc.a4.servlet;

import io.muic.ooc.a4.db.mysql.MySQLDB;
import io.muic.ooc.a4.utils.SessionValidatorUtil;
import io.muic.ooc.a4.utils.TemplatesUtil;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(TemplatesUtil.EditUser.SERVLET_PATH)
public class EditUserServlet extends HttpServlet{
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    if (!SessionValidatorUtil.validateCredentials(req, resp)) return;

    String[] params = StringUtils.replace(req.getRequestURI(), "/users/edit/", "").split("/", 2);

    if ((params.length < 1 && params[0].trim().length() == 0)) {
      resp.sendRedirect(TemplatesUtil.Users.SERVLET_PATH);
      return;
    }


    MySQLDB mySQLDB = new MySQLDB();
    Map<String, String> user = mySQLDB.getUserById(params[0]);
//    System.out.println(user);
    req.setAttribute("user", user);


    req.getRequestDispatcher(TemplatesUtil.EditUser.TEMPLATE_LOCATION).forward(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    if (!SessionValidatorUtil.validateCredentials(req, resp)) return;

    String id = req.getParameter("id");
    String firstname = req.getParameter("firstname");
    String lastname = req.getParameter("lastname");

    boolean allNotBlank = (StringUtils.isNotBlank(id) && StringUtils.isNotBlank(firstname)) && StringUtils.isNotBlank(lastname);

    MySQLDB mySQLDB = new MySQLDB();
    if (allNotBlank){
      mySQLDB.updateUserInfo(id, firstname, lastname);
      resp.sendRedirect(TemplatesUtil.Users.SERVLET_PATH);

    } else {

      req.setAttribute("error", "Invalid Data");
      req.setAttribute("user", mySQLDB.getUserById(id));
      req.getRequestDispatcher(TemplatesUtil.EditUser.TEMPLATE_LOCATION).include(req, resp);

    }





  }
}
