<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Edit User Info</title>
</head>
<body>
  <div class="container">
    <h2>Users Information</h2>
    ${error}
    <c:choose>
      <c:when test="${not empty user}">
        <form action="/users/edit/${user.id}" method="post">
          <input placeholder="First Name" value="${user.firstname}" name="firstname">
          <input placeholder="Last Name" value="${user.lastname}" name="lastname">
          <input type="hidden" value="${user.id}" name="id">
          <input type="submit" value="Save">
        </form>

      </c:when>
      
    </c:choose>

  </div>
</body>
</html>
