<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Users Information</title>

  <style>
    table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even) {
      background-color: #dddddd;
    }
  </style>

</head>
<body>
<div class="container">
  <h2>Users Information</h2>

  <c:choose>
    <c:when test="${not empty lstUsers}">
      <p>Logged In as ${currentUser}</p>
      <table>
        <tr>
          <th>ID</th>
          <th>Username</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th colspan="3" align="center">Action</th>
        </tr>
        <c:forEach var="user" items="${lstUsers}">

          <tr>
            <td>${user.id}</td>
            <td>${user.username}</td>
            <td>${user.firstname}</td>
            <td>${user.lastname}</td>
            <td><a href="/users/edit/${user.id}"><button>EDIT USER</button></a></td>
            <td><a href="/users/resetpassword/${user.id}" class="button"><button>RESET PASSWORD</button></a></td>
            <td><a href="/users/remove/${user.id}" onclick="return confirm('Are you sure you want to delete?');"><button>DELETE USER</button></a></td>
          </tr>
        </c:forEach>
      </table>
      <a href="/logout"><button>Sign Out</button></a>
    </c:when>
  </c:choose>
  <a href="/users/add"><button>Add User</button></a>
</div>
</body>
</html>
