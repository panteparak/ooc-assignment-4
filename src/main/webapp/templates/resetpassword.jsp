<%--
  Created by IntelliJ IDEA.
  User: panteparak
  Date: 2/17/17
  Time: 11:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Password Reset</title>
</head>
<body>
<p>Reset Password</p>

<form method="POST" action="/users/resetpassword/${userid}">
  <input placeholder="NEW PASSWORD" name="password" type="password">
  <input type="hidden" name="id" value="${userid}">
  <input type="submit" value="Save">
</form>
</body>
</html>
