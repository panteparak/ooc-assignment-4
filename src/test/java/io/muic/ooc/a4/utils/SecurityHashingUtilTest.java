package io.muic.ooc.a4.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by panteparak on 2/16/17.
 */
public class SecurityHashingUtilTest {
  @Test
  public void correctPassword() throws Exception {
    String plain = "HelloMUIC";
    String hash = SecurityHashingUtil.hashPassword("HelloMUIC");
    Assert.assertTrue(SecurityHashingUtil.verifyPassword(hash, plain));
  }

  @Test
  public void incorrectPassword() throws Exception {
    Assert.assertFalse(SecurityHashingUtil.verifyPassword(SecurityHashingUtil.hashPassword("122444"), "fffd"));
  }

}